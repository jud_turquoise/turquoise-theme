<nav id="navigation-menu" class="navbar navbar-expand-md bg-translucent">

  <div id="navbar-content" class="navbar-collapse collapse">
	  <?php
	  if ( has_nav_menu( 'navigation-menu' ) ) {
		  wp_nav_menu( array(
		  'container'      => 'ul',
		  'menu_class'     => 'menu navbar-nav mx-md-5',
		  'depth'          => '1',
		  'theme_location' => 'navigation-menu',
	  ) );
	  } else {
		  wp_page_menu( array(
		  'container'      => 'ul',
		  'menu_class'     => 'menu navbar-nav mx-md-5',
		  'depth'          => '1',
		  'show_home'      => true,
		  'before'         => '',
		  'after'          => '',
	  ) );
	  }
	  ?>
  </div>

  <button class="navbar-toggler collapsed" data-toggle="collapse" data-target="#navbar-content" aria-expanded="false">
    <svg id="i-menu"
         viewBox="0 0 32 32" width="32" height="32" fill="none"
         stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
      <path d="M30 12 L16 24 2 12"></path>
    </svg>
    <svg id="i-close"
         viewBox="0 0 32 32" width="32" height="32" fill="none"
         stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
      <path d="M30 20 L16 8 2 20"></path>
    </svg>
  </button>

</nav>
