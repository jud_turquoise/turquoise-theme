<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body <?php body_class(); ?>>

  <?php get_template_part('components/navbar'); ?>

  <div id="wide-container">
    <?php
    if (function_exists('the_custom_logo')) {
      the_custom_logo();
    }
    ?>
  </div>

  <?php get_footer('frontpage'); ?>
</body>

</html>



