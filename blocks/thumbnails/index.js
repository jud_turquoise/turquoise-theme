((wp) => {
    /**
     * Registers a new block provided a unique name and an object defining its behavior.
     * @see https://github.com/WordPress/gutenberg/tree/master/blocks#api
     */
    const {registerBlockType} = wp.blocks;
    const {withSelect} = wp.data;
    const {ServerSideRender} = wp.components;
    /**
     * Returns a new element of given type. Element is an abstraction layer atop React.
     * @see https://github.com/WordPress/gutenberg/tree/master/element#element
     */
    const el = wp.element.createElement;
    /**
     * Retrieves the translation of text.
     * @see https://github.com/WordPress/gutenberg/tree/master/i18n#api
     */
    const {__} = wp.i18n;

    /**
     * Every block starts by registering a new block type definition.
     * @see https://wordpress.org/gutenberg/handbook/block-api/
     */
    registerBlockType('turquoise/thumbnails', {
        title: __('Projects\' thumbnails'),
        icon: 'format-gallery',
        category: 'widgets',
        supports: {
            html: false, // Removes support for an HTML mode.
        },

        /**
         * The edit function describes the structure of your block in the context of the editor.
         * This represents what the editor will render when the block is used.
         * @see https://wordpress.org/gutenberg/handbook/block-edit-save/#edit
         *
         * @param {Object} [props] Properties passed from the editor.
         * @return {Element}       Element to render.
         */
        edit(props) {
            // ensure the block attributes matches this plugin's name
            return (
                el(ServerSideRender, {
                    block: "turquoise/thumbnails",
                    attributes: props.attributes
                })
            );
        },

        /**
         * The save function defines the way in which the different attributes should be combined
         * into the final markup, which is then serialized by Gutenberg into `post_content`.
         * @see https://wordpress.org/gutenberg/handbook/block-edit-save/#save
         *
         * @return {Element}       Element to render.
         */
        save() {
            // Rendering in PHP
            return null;
        },
    });
})(
    window.wp
);
