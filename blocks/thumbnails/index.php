<?php
/**
 * @param $attributes
 * @param $content
 * @return string
 */
function thumbnails_block_render($attributes, $content)
{
    $loop = new WP_Query(array(
        'post_type' => 'post',
    ));
    $output = '';
    if ($loop->have_posts()):
        while ($loop->have_posts()) : $loop->the_post();
            if (has_post_thumbnail()) {
                $featured_img_url = get_the_post_thumbnail_url(get_the_ID(), 'thumbnail_medium');
                $output .= '
                  <div class="col-md-4 col-xs-12">
                    <div class="card thumb-container">
                      <a href="' . get_permalink() . '">
                        <img class="card-img" src="' . $featured_img_url . '" alt="' . get_the_title() . '">
                        <div class="card-img-overlay thumb-overlay">
                          <h3 class="card-title thumb-description">' . get_the_title() . '</h3>
                        </div>
                      </a>
                    </div>
                  </div>
                ';
            }
        endwhile;
    endif;
    wp_reset_postdata();
    return '<div class="wp-block-turquoise-thumbnails container row align-items-center">' . $output . '</div>';
}

/**
 * Registers all block assets so that they can be enqueued through Gutenberg in
 * the corresponding context.
 *
 * @see https://wordpress.org/gutenberg/handbook/blocks/writing-your-first-block-type/#enqueuing-block-scripts
 */
function thumbnails_block_init()
{
    // Skip block registration if Gutenberg is not enabled/merged.
    if (!function_exists('register_block_type')) {
        return;
    }
    $dir = get_template_directory() . '/blocks';

    $index_js = 'thumbnails/index.js';
    wp_register_script(
        'thumbnails-block-editor',
        get_template_directory_uri() . "/blocks/$index_js",
        array(
            'wp-blocks',
            'wp-i18n',
            'wp-element',
        ),
        filemtime("$dir/$index_js")
    );

    $editor_css = 'thumbnails/editor.css';
    wp_register_style(
        'thumbnails-block-editor',
        get_template_directory_uri() . "/blocks/$editor_css",
        array(),
        filemtime("$dir/$editor_css")
    );

    $style_css = 'thumbnails/style.css';
    wp_register_style(
        'thumbnails-block',
        get_template_directory_uri() . "/blocks/$style_css",
        array(),
        filemtime("$dir/$style_css")
    );

    register_block_type('turquoise/thumbnails', array(
        'editor_script' => 'thumbnails-block-editor',
        'editor_style' => 'thumbnails-block-editor',
        'style' => 'thumbnails-block',
        'render_callback' => 'thumbnails_block_render',
    ));
}

add_action('init', 'thumbnails_block_init');
