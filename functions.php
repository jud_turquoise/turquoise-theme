<?php

function turquoise_scripts()
{
    wp_enqueue_script(
        'bootstrap-js',
        get_template_directory_uri() . '/dist/bootstrap-4.1.3-dist/js/bootstrap.min.js', array('jquery'),
        '4.1.3',
        true
    );
}

add_action('wp_enqueue_scripts', 'turquoise_scripts');

function turquoise_styles()
{
    wp_enqueue_style(
        'bootstrap',
        get_template_directory_uri() . '/dist/bootstrap-4.1.3-dist/css/bootstrap.min.css', array(),
        '4.1.2'
    );
    wp_enqueue_style('style', get_stylesheet_uri());
}

add_action('wp_print_styles', 'turquoise_styles');

function turquoise_fonts()
{
    wp_register_style('Lato', '//fonts.googleapis.com/css?family=Lato');
    wp_enqueue_style('Lato');
    wp_register_style('Raleway', '//fonts.googleapis.com/css?family=Raleway');
    wp_enqueue_style('Raleway');
    wp_register_style('Cutive', '//fonts.googleapis.com/css?family=Cutive+Mono');
    wp_enqueue_style('Cutive');
}

add_action('wp_print_styles', 'turquoise_fonts');

add_theme_support('title-tag');

// Support Featured Images
add_theme_support('post-thumbnails');

/**
 * Background image configuration
 */
add_theme_support('custom-background', array(
    'default-color' => 'eaebed',
    'default-image' => get_parent_theme_file_uri('/img/default-background.jpg'),
    'default-repeat' => 'no-repeat',
    'default-position-x' => 'center',
    'default-position-y' => 'top',
    'default-size' => 'cover',
    'default-attachment' => 'fixed',
    'wp-head-callback' => '_custom_background_cb',
    'admin-head-callback' => '',
    'admin-preview-callback' => ''
));

function turquoise_setup_images()
{
    add_image_size('thumbnail_medium', 450, 450, true);
}

add_action('after_setup_theme', 'turquoise_setup_images');

/**
 * Add Custom Logo support
 */
function turquoise_custom_logo_setup() {
  $defaults = array(
    'height'      => 160,
    'width'       => 160,
    'flex-height' => true,
    'flex-width'  => true,
    'header-text' => array( 'site-title', 'site-description' ),
  );
  add_theme_support( 'custom-logo', $defaults );
}
add_action( 'after_setup_theme', 'turquoise_custom_logo_setup' );

/**
 * Register Menus
 */

function turquoise_register_menus()
{
    register_nav_menus(
        array(
            'navigation-menu' => __('Navigation Menu'),
        )
    );
}

add_action('init', 'turquoise_register_menus');

/**
 * Gutenberg blocks
 */
include 'blocks/thumbnails/index.php';
