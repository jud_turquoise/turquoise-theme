<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body <?php body_class('no-bg'); ?>>
  <?php get_template_part('components/navbar'); ?>

  <div class="container" style="min-height: 960px">

    <div class="entry-attachment">

      <?php $image_size = apply_filters('wporg_attachment_size', 'large'); ?>

      <div class="navigation-attachment d-block mx-auto">

        <a href="<?php echo get_permalink($post->post_parent); ?>">
          <span class="icon">
            <svg id="i-arrow-left" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
              <path d="M10 6 L2 16 10 26 M2 16 L30 16"/>
            </svg>
          </span>
        </a>

        <?php
        previous_image_link(
          $image_size,
          '<span class="icon">
            <svg id="i-chevron-left" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
              <path d="M20 30 L8 16 20 2" />
            </svg>
          </span>'
        );
        ?>

        <?php
        next_image_link(
          $image_size,
          '<span class="icon">
            <svg id="i-chevron-right" viewBox="0 0 32 32" width="32" height="32" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
              <path d="M12 30 L24 16 12 2" />
            </svg>
          </span>'
        );
        ?>

      </div>

      <?php
      echo wp_get_attachment_image(
        get_the_ID(),
        $image_size,
        false,
        array("class" => "img-fluid rounded d-block")
      );
      ?>

      <?php if (has_excerpt()) : ?>
        <div class="entry-caption">
          <?php the_excerpt(); ?>
        </div>
      <?php endif; ?>
    </div>

  </div>

  <?php get_footer(); ?>
</body>

</html>