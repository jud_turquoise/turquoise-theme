<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body <?php body_class('no-bg'); ?>>

    <?php get_template_part('components/navbar'); ?>

  <div class="container" style="min-height: 960px">
      <?php
      if (have_posts()) : while (have_posts()) : the_post(); ?>
        <div class="blog-post">
          <h2 class="blog-post-title border-bottom"><?php the_title(); ?></h2>
          <p class="blog-post-meta"><?php the_date(); ?></a></p>
            <?php the_content(); ?>

        </div>
      <?php

      endwhile; endif;
      ?>
  </div>

    <?php get_footer(); ?>
</body>

</html>