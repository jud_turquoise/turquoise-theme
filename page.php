<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<?php get_header(); ?>

<body <?php body_class('no-bg'); ?>>
	<?php get_template_part( 'components/navbar' ); ?>

  <div class="container" style="min-height: 800px">
	  <?php
	  if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="blog-post px-0 px-md-3">
          <?php the_content(); ?>
      </div>
    <?php
	  endwhile; endif;
	  ?>
  </div>

	<?php get_footer(); ?>
</body>

</html>